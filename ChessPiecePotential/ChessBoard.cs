﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessPiecePotential
{
    class ChessBoard
    {
        private int _x;
        private int _y;

        public ChessBoard(int x, int y)
        {
            _x = Math.Max(x, 3);
            _y = Math.Max(y, 3);
        }

        public string ToString()
        {
            StringBuilder sb = new StringBuilder();
            string header = string.Concat(Enumerable.Repeat(" _", _y));
            string squares = string.Concat(Enumerable.Repeat("|_", _y));
            squares += '|';

            for (int i = 0; i <= _x; i++)
            {
                if (i == 0)
                {
                    sb.Append(header);
                    sb.Append('\n');
                }
                sb.Append(squares);
                sb.Append('\n');
            }
            return sb.ToString();
        }

        public static string TestToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\n=========================================\n");
            Random r = new Random();
            for (int i = 0; i <= 5; i++)
            {
                int x = r.Next(0, 8);
                int y = r.Next(0, 8);
                ChessBoard c = new ChessBoard(x, y);
                sb.Append(c.ToString());
                sb.Append("\n=========================================\n");
            }

            return sb.ToString();
        }
    }
}
