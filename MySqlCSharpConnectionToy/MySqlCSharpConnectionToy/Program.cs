﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;



namespace MySqlCSharpConnectionToy
{
    class Program
    {
        static void Main(string[] args)
        {
            string server = "";
            string database = "";
            string username = "";
            string password = "";

            string tablename = "";
            string coltogroupby = "";

            MySQLdb db = new MySQLdb(server, database, username, password);

            List<string> wheres = GetWheres();
            List<DataRow> response = db.CountDistinctInColumn(tablename, coltogroupby, wheres);

            string output = StringifyOutput(response);
            Console.Write(output);
            Console.ReadKey();
        }

        private static string StringifyOutput(List<DataRow> response)
        {
            StringBuilder sb = new StringBuilder();
            response.ForEach(x => sb.AppendLine(x.RowAsString()));
            string output = sb.ToString();
            return output;
        }

        private static List<string> GetWheres()
        {
            string today = DateTime.UtcNow.ToString("yyyy-MM-dd");
            string yesterday = DateTime.UtcNow.AddDays(-1).ToString("yyyy-MM-dd"); ;

            string voted = "voted = 1";
            string startdate = String.Format("created_at > '{0}'", yesterday);
            string enddate = String.Format("created_at <= '{0}'", today);
            List<string> wheres = new List<string>();
            wheres.Add(voted);
            wheres.Add(startdate);
            wheres.Add(enddate);
            return wheres;
        }
    }
}
