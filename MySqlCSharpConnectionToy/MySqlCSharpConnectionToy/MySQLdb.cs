﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using System.Configuration;
using System.Collections.Specialized;
using MySql.Data.MySqlClient;

namespace MySqlCSharpConnectionToy
{
    public static class ExtMethods
    {
        public static string RowAsString(this DataRow r)
        {
            return String.Join(", ", r.ItemArray);
        }
    }

    public class MySQLdb
    {
        private Object connectionLock;
        private MySqlConnection connection;
        private string _server;
        private string _database;
        private string _username;
        private string _password;

        public MySQLdb(string server, string database, string username = null, string password = null)
        {
            _server = server;
            _database = database;
            _username = username;
            _password = password;
            connectionLock = new Object();
            Init();
        }

        public MySQLdb()
        {
            _server = ConfigurationManager.AppSettings.Get("server");
            _database = ConfigurationManager.AppSettings.Get("db");
            _username = ConfigurationManager.AppSettings.Get("username");
            _password = ConfigurationManager.AppSettings.Get("password");
            connectionLock = new Object();
            Init();
        }


        private void Init()
        {
            string cstring = String.Format(@"SERVER=""{0}"";DATABASE=""{1}"";UID=""{2}"";PASSWORD=""{3}"";", _server, _database, _username, _password);
            lock (connectionLock)
            {
                connection = new MySqlConnection(cstring);
            }
        }

        private bool Connect()
        {
            try
            {
                lock (connectionLock)
                {
                    connection.Open();
                }
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }

        private bool Close()
        {
            try
            {
                lock (connectionLock)
                {
                    connection.Close();
                }
                return true;
            }
            catch (MySqlException) 
            {
                return false;
            }
        }

        public void Insert(string tableName, List<Tuple<string, string>> colValuePair)
        {
            const string action = "INSERT INTO";
            string toinsert = String.Join(",", colValuePair.Select(x => String.Format("{0}='{1}'", x.Item1, x.Item2)));
            string query = string.Format("{0} {1} SET {2}", action, tableName, toinsert);

            lock (connectionLock)
            {
                if (Connect() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    Close();
                }
            }
        }

        public void Update(string tableName, List<Tuple<string, string>> newValues, List<Tuple<string, string>> whereClause)
        {
            //"UPDATE tableinfo SET [newvalues] WHERE [whereClause]"; 
            const string action = "UPDATE";
            string toinsert = String.Join(",", newValues.Select(x => String.Format("{0}='{1}'", x.Item1, x.Item2)));
            string where = String.Join(",", whereClause.Select(x => String.Format("{0}='{1}'", x.Item1, x.Item2)));
            string query = string.Format("{0} {1} SET {2} WHERE {3}", action, tableName, toinsert, where);

            lock (connectionLock)
            {
                if (Connect() == true)
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandText = query;
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();

                    Close();
                }
            }
        }

        public void Delete(string tableName, List<string> whereClause)
        {
            string query = "DELETE FROM [tablename] WHERE [whereClause]"; //TODO: Finsih when needed

            lock (connectionLock)
            {
                if (Connect() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    Close();
                }
            }
        }

        public List<DataRow> Select(string query)
        {
            List<DataRow> toRet = null;

            lock (connectionLock)
            {
                if (Connect() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    MySqlDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    toRet = dt.AsEnumerable().ToList();
                    dr.Close();
                    Close();
                }
            }
            return toRet;
        }

        public List<DataRow> CountDistinctInColumn(string tableName, string colName, List<string> whereClause)
        {
            string where = String.Join(" AND ", whereClause);
            string query = String.Format(@"SELECT {1}, COUNT({1}) AS count
                             FROM {0} 
                             WHERE {2}
                             GROUP BY {1}", tableName, colName, where);
            return Select(query);
        }
    }
}
